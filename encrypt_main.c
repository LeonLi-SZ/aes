//=============================================================================
//  DESCRIPTION::
//      AES encryption for C8051
//
//  AUTHOR:
//      Leon Li             Feb 28, 2008
//
//  NOTICE:
//      The information contained herein is confidential and is proprietary information
//      of Microsoft Corporation, and shall not, without Microsoft Corporation's prior
//      written approval, be reproduced or in any way used in whole or in part in
//      connection with services or equipment offered for sale or furnished to others.
//      The information contained herein may not be disclosed to a third party without
//      consent of Microsoft Corporation, and then, only pursuant to a Microsoft
//      approved non-disclosure agreement. Microsoft assumes no liability for
//      incidental or consequential damages arising from the use of the information
//      contained herein, and reserves the right to update, revise, or change any
//      information in this document without notice.
//
//      2008 Microsoft Corporation. All rights reserved
//=============================================================================


#include "reg51.h"
#include "AES_encrypt.h"


// 2 plain text examples
#if 1
data uchar AES_Buffer[4*4] = {
    0x32, 0x88, 0x31, 0xe0,
    0x43, 0x5a, 0x31, 0x37,
    0xf6, 0x30, 0x98, 0x07,
    0xa8, 0x8d, 0xa2, 0x34,
};

#else
data uchar AES_Buffer[4*4] = {
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0xaa,
};
#endif



//2 cipher key examples
#if 1
static CIPHER_KEY_LOCATION uchar AES_Cipher_Key[4*4] = {
    0x2b, 0x28, 0xab, 0x09,
    0x7e, 0xae, 0xf7, 0xcf,
    0x15, 0xd2, 0x15, 0x4f,
    0x16, 0xa6, 0x88, 0x3c,
};

#else
static CIPHER_KEY_LOCATION uchar AES_Cipher_Key[4*4] = {
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0xbb,
};
#endif



void main (void) {
    while(1) {
        AES_Encrypte (&AES_Cipher_Key[0]);
        P1 = AES_Cipher_Key[3];                 // stop here to see final encrypt result
    }

}


//=============================================================================
//  DESCRIPTION::
//      AES decryption for C8051
//
//  AUTHOR:
//      Leon Li             Feb 28, 2008
//
//  NOTICE:
//      The information contained herein is confidential and is proprietary information
//      of Microsoft Corporation, and shall not, without Microsoft Corporation's prior
//      written approval, be reproduced or in any way used in whole or in part in
//      connection with services or equipment offered for sale or furnished to others.
//      The information contained herein may not be disclosed to a third party without
//      consent of Microsoft Corporation, and then, only pursuant to a Microsoft
//      approved non-disclosure agreement. Microsoft assumes no liability for
//      incidental or consequential damages arising from the use of the information
//      contained herein, and reserves the right to update, revise, or change any
//      information in this document without notice.
//
//      2008 Microsoft Corporation. All rights reserved
//=============================================================================



#include "AES_decrypt.h"

#ifdef  USE_KEIL_C51
    #include "reg51.h"
#endif


// 2 cipher text examples
#if 1
VAR_LOCATION uchar AES_Buffer[4*4] = {
    0x39, 0x02, 0xdc, 0x19,
    0x25, 0xdc, 0x11, 0x6a,
    0x84, 0x09, 0x85, 0x0b,
    0x1d, 0xfb, 0x97, 0x32,
};

#else
VAR_LOCATION uchar AES_Buffer[4*4] = {
    0x00, 0x02, 0xdc, 0x19,
    0x25, 0xdc, 0x11, 0x6a,
    0x84, 0x09, 0x85, 0x0b,
    0x1d, 0xfb, 0x97, 0x32,
};
#endif



//2 cipher key examples
#if 1
static CIPHER_KEY_LOCATION uchar AES_Cipher_Key[4*4] = {
    0x2b, 0x28, 0xab, 0x09,
    0x7e, 0xae, 0xf7, 0xcf,
    0x15, 0xd2, 0x15, 0x4f,
    0x16, 0xa6, 0x88, 0x3c,
};

#else
static CIPHER_KEY_LOCATION uchar AES_Cipher_Key[4*4] = {
    0x2b, 0x28, 0xab, 0x09,
    0x7e, 0xae, 0xf7, 0xcf,
    0x15, 0xd2, 0x15, 0x4f,
    0x16, 0xa6, 0x88, 0xab,
};
#endif



void main (void) {
    KeyExpanding(&AES_Cipher_Key[0]);           // to get ExpandedKey
    while(1) {
        AES_Decrypt ();
        #ifdef  USE_KEIL_C51
            P1 = AES_Buffer[3];                 // stop here to see final decrypt result
        #endif
    }

}

